[TOC]

# 1-1 Python介绍

## 一. Python的特点

优点：

* 简单明了、容易上手、功能强大
* 社区活跃、用户基数大、开发效率高
* 入门难度低

<br/>

缺点：

* 运行效率低

![img](https://gitee.com/paultest/Images/raw/master/img/109975cd-f933-447d-b775-7b436c1993c1.jpg)

<br/>

## 二. Python发展历史

如下图：

![image-20210421223426741](https://gitee.com/paultest/Images/raw/master/img/image-20210421223426741.png)

注意：Python 3.x版本是不兼容2.x版本的，二者只能选择其一，且2022年Python官方停止对2.x版本进行维护，所以尽量选择Python 3.x版本的。

<br/>

## 三. Python的现状

Python适合的领域：

* 云基础设施
* DevOps
* 后台开发
* 网络爬虫
* 金融行业
* 运营运维
* 数据分析
* 数据挖掘
* 人工智能

<br/>

Python不适合的领域：

- 底层应用（驱动等）：贴近硬件的代码（首选C）
- 移动开发：iOS/Android有各自的开发语言（ObjC，Swift，Java）
- 游戏开发：C/C++

<br/>

# 2-1 安装Python

Python程序是需要运行在Python环境下的，而Python环境是依赖于操作系统的，如下图：

![image-20210421224531607](https://gitee.com/paultest/Images/raw/master/img/image-20210421224531607.png)

Python可以运行在Windows操作系统也可以运行在Linux如CentOS操作系统下的

这里只介绍这两种操作系统的安装方式

## 一. Windows安装

### 1. 下载Python

去[官网](https://www.python.org/)下载Python3，如下图，下载好了之后进行安装即可

![image-20210421225102370](https://gitee.com/paultest/Images/raw/master/img/image-20210421225102370.png)

注意：目前最新的Python版本是3.9.4

注意：在安装过程中，会提示要不要加入到Path，建议勾选，默认是没有勾选的，如下图：

![image-20210421230444080](https://gitee.com/paultest/Images/raw/master/img/image-20210421230444080.png)

<br/>

### 2. 配置环境

如果安装过程中没有勾选加入到Path的话，那也可以手动进行配置环境

**注意：如安装过程勾选的话，该步骤跳过**

右击我的电脑-属性-高级系统设置-环境变量-系统变量-path-编辑-新建-输入python安装路径即可

![image-20210421230126417](https://gitee.com/paultest/Images/raw/master/img/image-20210421230126417.png)

![image-20210421230201154](https://gitee.com/paultest/Images/raw/master/img/image-20210421230201154.png)

<br/>

### 3. 验证安装成功

打开cmd，输入python即可

![image-20210421225838873](https://gitee.com/paultest/Images/raw/master/img/image-20210421225838873.png)

![image-20210421225936333](https://gitee.com/paultest/Images/raw/master/img/image-20210421225936333.png)

注意：我自己安装的是Python 3.7

<br/>

## 二. Centos 7 安装

具体可参考：[【Python3】CentOS7安装Python3【原创】](https://blog.csdn.net/jiandanokok/article/details/96511379)

<br/>

由于CentOS7原本就安装了Python2，而且这个Python2不能被删除，因为有很多系统命令，比如yum都要用到

<br/>

输入Python命令，查看可以得知是Python2.7.5版本

```shell
python
Python 2.7.5 (default, Aug  4 2017, 00:39:18) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-16)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
```

<br/>

输入which python：

```shell
which python
/usr/bin/python
```

可以查看位置，一般是位于/usr/bin/python目录下

<br/>

### 1. 安装依赖包


```shell
yum -y groupinstall "Development tools"
yum -y install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel libffi-devel
```

<br/>

### 2. 下载Python3
然后根据自己需求下载不同版本的Python3，我下载的是Python3.7.4

```shell
wget https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tar.xz
```

如果速度不够快，可以直接去官网下载，利用WinSCP等软件传到服务器上指定位置

<br/>

### 3. 编译

先建立一个空文件夹：

```shell
mkdir /usr/local/python3 
```

<br/>

解压压缩包：

```shell
tar -xvJf  Python-3.7.4.tar.xz
```

<br/>

进入该目录：

```shell
cd Python-3.7.4
```

<br/>

编译安装：

```shell
./configure --prefix=/usr/local/python3
make && make install
```

<br/>

创建软链接：

```shell
ln -s /usr/local/python3/bin/python3 /usr/bin/python3
ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
```

<br/>

### 4. 测试
输入python3测试：

```shell
python3
Python 3.7.4 (default, Jul 19 2019, 19:28:25) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-36)] on linux
Type "help", "copyright", "credits" or "license" for more information.
```

<br/>

注意：pip是默认Python2.7的，需要使用pip3，这个才是Python3.7的
比如安装requests：

```shell
pip3 install requests
```

<br/>

# 2-2 第一个Python程序

把Python环境搭建完成之后，就可以尝试运行Python程序了。

运行Python程序有两种方式：

* 通过命令行编写代码运行
* 通过编辑器编写代码运行

<br/>

对于程序员来说，学习一门新的语言，第一步是通过这门语言向世界问好：“Hello World”。

<br/>

注意：建议使用Virtualenv来建立一个虚拟环境，可参考：[【Virtualenv】Python的虚拟环境Virtualenv和Virtualenvwrapper](https://blog.csdn.net/jiandanokok/article/details/102768561)

<br/>

代码仓库地址：https://gitee.com/paultest/python-study.git

<br/>

## 一、在命令行写代码

在cmd中输入python即可进去python交互环境，然后输入print ‘hello world’回车之后也可以有同样的效果，不过缺点是不可以保存

注意：退出python交互环境，使用`exit()`或者是crtl + z按键即可退出

<br/>

步骤：

1. 打开命令行窗口（通过快捷键`Win+R`打开运行窗口，输入`cmd`即可进入命令行窗口）
2. 输入`python`进入python运行环境（正确的python运行环境会有`>>>`的提示）
3. 输入你的第一行Python代码`print('Hello World')`，回车即可运行，第一次运行，有几个需要注意的地方：
    - 注意在`Hello World`前后都有单引号`'`
    - `print`和`>>>`不可以有空格
    - 注意`(`、`)`、`'`、`'`均为英文字符

如图：

![image-20210421231804454](https://gitee.com/paultest/Images/raw/master/img/image-20210421231804454.png)

<br/>

## 二、在编辑器写代码

常见的编辑器包括`Sublime Text3`，`Visual Code`，`PyCharm`等。

<br/>

创建一个py文件，2-2.hello_world.py：

```python
# -*- encoding: utf-8 -*-
"""
@Time    : 2021/4/21 23:12
@Author  : boli.hong
"""
print('Hello World')
```

注意：首行行首不能有空格，python对于缩进十分严格

<br/>

然后通过cmd，进入保存文件的路径，然后`python 2-2.hello_world.py`即可：

![image-20210421232159343](https://gitee.com/paultest/Images/raw/master/img/image-20210421232159343.png)

 <br/>

# 3-1 数据类型

 <br/>

## 参考

* [Python3 入门教程](https://www.imooc.com/learn/1261)

* [Python3 进阶教程](https://www.imooc.com/learn/1264)
* [Python官网](https://www.python.org/)
* [代码仓库](https://gitee.com/paultest/python-study.git)

